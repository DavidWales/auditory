﻿using UnityEngine;

public class ControllerInput : InputSystem
{
    private Vector2 _middleOfScreen;
    private float _radius;
    private Reticle _reticle;

    public ControllerInput()
    {
        _radius = 1f;
        _reticle = GameObject.FindObjectOfType<Reticle>();
    }

    public override Vector2 GetCirclePosition()
	{
		Vector2 circlePos = new Vector2(Input.GetAxisRaw("RightAnalogXAxis"), Input.GetAxisRaw("RightAnalogYAxis"));

        circlePos = (circlePos.magnitude > _radius)
            ? circlePos.normalized //Outside bounds of circle
            : circlePos / _radius; //Inside bounds of circle

        if (_reticle != null)
        {
            _reticle.SetPosition(circlePos);
        }

        //Debug.Log(circlePos);
        return circlePos;
    }

	public override int GetSelectedDoorID()
	{
        if (Input.GetAxisRaw("DPadHorizontal") <= -1f)
        {
            return 1;
        }
        else if (Input.GetAxisRaw("DPadVertical") >= 1f)
        {
            return 2;
        }
        else if (Input.GetAxisRaw("DPadHorizontal") >= 1f)
        {
            return 3;
        }
        else
        {
            return -1;
        }
    }

    public override bool GameStartButtonDetected()
    {
        return Input.GetKeyDown(KeyCode.Joystick1Button7);
    }
}