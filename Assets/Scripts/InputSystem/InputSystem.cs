﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class InputSystem
{
	public abstract Vector2 GetCirclePosition();
	public abstract int GetSelectedDoorID();
    public abstract bool GameStartButtonDetected();
}