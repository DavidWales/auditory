﻿using UnityEngine;

public class MouseKeyboardInput : InputSystem
{
	private Vector2 _middleOfScreen;
	private float _radius;
	private Reticle _reticle;

	public MouseKeyboardInput()
	{
		Cursor.visible = false;

		_middleOfScreen = new Vector2(Screen.width / 2.0f, Screen.height / 2.0f);
		_radius = ((Screen.width > Screen.height) ? Screen.height : Screen.width) / 2.0f;
		_reticle = GameObject.FindObjectOfType<Reticle>();
	}

	public override Vector2 GetCirclePosition()
	{
		Vector2 circlePos = ((Vector2)Input.mousePosition - _middleOfScreen);
		circlePos = (circlePos.magnitude > _radius)
			? circlePos.normalized //Outside bounds of circle
			: circlePos / _radius; //Inside bounds of circle

		if(_reticle != null)
		{
			_reticle.SetPosition(circlePos);
		}

		return circlePos;
	}

	public override int GetSelectedDoorID()
	{
		if (Input.GetKeyDown(KeyCode.LeftArrow))
		{
			return 1;
		}
		else if (Input.GetKeyDown(KeyCode.UpArrow))
		{
			return 2;
		}
		else if(Input.GetKeyDown(KeyCode.RightArrow))
		{
			return 3;
		}
		else
		{
			return -1;
		}
	}

    public override bool GameStartButtonDetected()
    {
        return Input.anyKeyDown;
    }
}