﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Reticle : MonoBehaviour
{
	private RawImage _parentCircle;
	private float _parentRadius;

	private void Awake()
	{
		_parentCircle = this.transform.parent.GetComponent<RawImage>();
		_parentRadius = _parentCircle.rectTransform.sizeDelta.x / 2.0f;
	}

	public void SetPosition(Vector2 pos)
	{
		this.transform.localPosition = pos * _parentRadius;
	}

}