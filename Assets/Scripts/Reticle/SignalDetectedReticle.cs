﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SignalDetectedReticle : MonoBehaviour
{
	private RawImage _reticle;
    private float savedAlpha;

	private void Awake()
	{
        this.savedAlpha = this.GetComponent<RawImage>().color.a;
		_reticle = this.GetComponent<RawImage>();
	}

	public void SetReticleActive()
	{
		Color green = Color.green;
        green.a = this.savedAlpha;
		_reticle.color = green;
	}

	public void SetReticleInactive()
	{
		Color yellow = Color.yellow;
        yellow.a = this.savedAlpha;
		_reticle.color = yellow;
	}
}