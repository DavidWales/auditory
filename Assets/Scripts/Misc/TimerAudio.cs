﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimerAudio : MonoBehaviour
{
	public float minVolume = 0.0f;
	public float maxVolume = 1.0f;
	public float fadeStep = 2.0f;

	private AudioSource _audioSource;

	private void Awake()
	{
		_audioSource = this.GetComponent<AudioSource>();
		SetVolume(_audioSource.volume);
	}

	private void Update()
	{
		RoomManager roomManager = GameManager.GetCurrentRoomManager();
		if (roomManager != null)
		{
			if (roomManager.isActive == true)
			{
				SetVolume((maxVolume - minVolume) * (1.0f - (roomManager.GetTimerRemaining() / roomManager.GetInitialTimer())) + minVolume);
			}
			else
			{
				SetVolume(_audioSource.volume - (fadeStep * Time.deltaTime));
			}
		}
	}

	public void SetVolume(float newVolume)
	{
		_audioSource.volume = newVolume;
		if (_audioSource.volume > maxVolume)
		{
			_audioSource.volume = maxVolume;
		}
		else if (_audioSource.volume < minVolume)
		{
			_audioSource.volume = minVolume;
		}
	}
}