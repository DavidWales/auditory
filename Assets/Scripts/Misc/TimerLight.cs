﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimerLight : MonoBehaviour
{
	public RoomManager roomManager;

	private Light _light;
	private float _initialIntensity;
	private bool _isActive = true;

	private void Awake()
	{
		_light = this.GetComponent<Light>();
		_initialIntensity = _light.intensity;
	}

	private void Update()
	{
		if(roomManager.isActive && _isActive == true)
		{
			_light.intensity = _initialIntensity * (roomManager.GetTimerRemaining() / roomManager.GetInitialTimer());
			if(_light.intensity <= 0.0f)
			{
				_isActive = false;
			}
		}
		else if(roomManager.isActive == false && _light.intensity <= 0.0f)
		{
			_light.intensity += Time.deltaTime;
		}
	}
}