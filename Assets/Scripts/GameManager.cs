﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public Fader blackScreenFader;
    public Fader gameWonFader;
    public RawImage gameStart;
    public Fader gameStartFader;
    public GameObject startRoom;

	private RoomManager _roomManager;
	private PlayerController _playerController;
    private bool _gameStarted = false;
    private AudioSource _loseAudioSource;

	public static void SetCurrentRoomManager(RoomManager roomManager)
	{
		_inst._roomManager = roomManager;
	}

	public static RoomManager GetCurrentRoomManager()
	{
		return _inst._roomManager;
	}

    private void Start()
    {
        _inst.gameStart.color = new Color
        {
            r = _inst.gameStart.color.r,
            g = _inst.gameStart.color.g,
            b = _inst.gameStart.color.b,
            a = 1.0f
        };
        _gameStarted = false;
        RoomFactory.SetupRoom(_inst.startRoom);
    }

    public static void StartGame()
    {
        _inst.gameStartFader.FadeOut(() =>
        {
            PathingManager.path = true;
            _inst._gameStarted = true;
        });
    }

    public static void GameWon()
	{
        _inst._playerController.isActive = false;
        _inst.gameWonFader.FadeIn(() =>
        {
            SceneManager.LoadScene(0);
        });
	}

	public static void GameLost()
	{
        _inst._gameStarted = false;
        _inst._playerController.isActive = false;
        _inst._loseAudioSource.Play();
        _inst.blackScreenFader.FadeIn(() =>
        {
            SceneManager.LoadScene(0);
        });
	}

    public static bool IsGameStarted()
    {
        return _inst._gameStarted;
    }

	#region Singleton Stuff

	public static GameManager _inst = null;

	void Awake()
	{
		if (_inst != null)
		{
            Destroy(this.gameObject);
			return;
		}
		_inst = this;
        _loseAudioSource = this.GetComponent<AudioSource>();
        _playerController = GameObject.FindObjectOfType<PlayerController>();
	}

	#endregion

}