﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
	public InputSystemEnum inputSystemType = InputSystemEnum.MOUSEKEYBOARD;

	private InputSystem _inputSystem;
	private AnalogSoundManager _analogSoundManager;

	public bool isActive { get; set; }

	private void Awake()
	{
		switch(inputSystemType)
		{
			case InputSystemEnum.MOUSEKEYBOARD:
				_inputSystem = new MouseKeyboardInput();
				break;
			case InputSystemEnum.CONTROLLER:
				_inputSystem = new ControllerInput();
				break;
			default:
				Debug.LogError("Invalid Player InputSystem");
				break;
		}
		_analogSoundManager = GameObject.FindObjectOfType<AnalogSoundManager>();
		isActive = false;
	}

	private void Update()
	{
        if(GameManager.IsGameStarted() == false)
        {
            if (_inputSystem.GameStartButtonDetected())
            {
                GameManager.StartGame();
            }
        }

		if (isActive)
		{
			Vector2 analogPos = _inputSystem.GetCirclePosition();
			AnalogSoundManager.SetAudioSourcePropertiesByAnalog(analogPos);

			int selectedDoor = _inputSystem.GetSelectedDoorID();
			if (selectedDoor != -1)
			{
				GameManager.GetCurrentRoomManager().MakeDoorSelection(selectedDoor);
			}
		}
	}
}