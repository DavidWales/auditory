﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorPathingNode : PathingNode
{
    public Door associatedDoor;

    public override void ReachedNode()
    {
        base.ReachedNode();
        associatedDoor.ToggleDoor(() =>
        {
            PathingManager.path = true;
        });
    }
}