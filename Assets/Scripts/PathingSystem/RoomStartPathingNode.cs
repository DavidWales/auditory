﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomStartPathingNode : PathingNode
{
	public RoomManager roomManager;

	public override void ReachedNode()
	{
		base.ReachedNode();
		roomManager.StartRoomChallenge();
	}
}