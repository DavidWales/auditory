﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathingManager : MonoBehaviour
{
	public PathingNode firstPathingNode;
	public Transform player;
	public float playerMoveSpeed = 10.0f;
	public float playerRotateSpeed = 15.0f;

	private float? _prevPlayerMoveSpeed = null;
	private float? _prevPlayerRotateSpeed = null;
	public PathingNode _currentPathNode = null;

	public static bool path { get; set; }

	private void Update()
	{
		if(path == true && _currentPathNode != null)
		{
			Vector3 dirToNextNode = (_currentPathNode.transform.position - player.transform.position).normalized;

			//Rotate Player
			Quaternion goalRot = Quaternion.LookRotation(dirToNextNode);
			if (Quaternion.Angle(player.transform.rotation, goalRot) > 0.1f)
			{
				player.transform.rotation = Quaternion.Lerp(
					player.transform.rotation,
					goalRot,
					playerRotateSpeed * Time.deltaTime);
			}

			//Determine if we have reached the goal
			if (Vector3.Distance(player.transform.position, _currentPathNode.transform.position) < 0.1f)
			{
				//Set Movement Variables To Original
				if (_prevPlayerMoveSpeed.HasValue)
				{
					playerMoveSpeed = _prevPlayerMoveSpeed.Value;
				}
				if (_prevPlayerRotateSpeed.HasValue)
				{
					playerRotateSpeed = _prevPlayerRotateSpeed.Value;
				}

				_currentPathNode.ReachedNode();
			}
			else
			{
				//Move Player
				player.transform.position = Vector3.MoveTowards(
					player.transform.position,
					_currentPathNode.transform.position,
					playerMoveSpeed * Time.deltaTime);
			}
		}
	}

	public static void SetNextNode()
	{
        if (_inst._currentPathNode.victoryNode == true)
        {
            GameManager.GameWon();
        }
        else if (_inst._currentPathNode.nextNode != null)
		{
            _inst._currentPathNode = _inst._currentPathNode.nextNode;
		}
	}

    public static void SetNextNode(PathingNode node)
    {
        _inst._currentPathNode.nextNode = node;
        SetNextNode();
    }

	public static void SetNodePlayerSettings(float moveSpeed, float rotateSpeed)
	{
		if (moveSpeed != 0.0f)
		{
			_inst._prevPlayerMoveSpeed = _inst.playerMoveSpeed;
            _inst.playerMoveSpeed = moveSpeed;
		}
		if (rotateSpeed != 0.0f)
		{
            _inst._prevPlayerRotateSpeed = _inst.playerRotateSpeed;
            _inst.playerRotateSpeed = rotateSpeed;
		}
	}

    #region Singleton Stuff

    public static PathingManager _inst = null;

    void Awake()
    {
        if (_inst != null)
        {
            Destroy(this.gameObject);
            return;
        }
        _inst = this;

        if (firstPathingNode != null)
        {
            _currentPathNode = firstPathingNode;
            player.transform.position = firstPathingNode.transform.position;
            path = false;
        }
    }

    #endregion
}