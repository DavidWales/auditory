﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathingNode : MonoBehaviour
{
	public PathingNode nextNode = null;
	public bool autoContinue = true;
	public float playerMoveSpeed;
	public float playerRotateSpeed;
    public bool victoryNode = false;

	public virtual void ReachedNode()
	{
		PathingManager.path = (autoContinue == true) ? true : false;
        PathingManager.SetNextNode();
        PathingManager.SetNodePlayerSettings(playerMoveSpeed, playerRotateSpeed);
	}

}