﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{
	public Vector3 openAngle;
	public float rotationSpeed;
	public AudioClip[] openClips;

	private Quaternion openQuat;
	private Quaternion startQuat;
	private bool _isActive;
	private bool _isOpening;
	private Action _callback;
	private AudioSource _audioSource;

	private void Awake()
	{
		startQuat = this.transform.rotation;
		openQuat = Quaternion.Euler(openAngle);
		_isOpening = false;
		_isActive = false;
		_audioSource = this.GetComponent<AudioSource>();
	}

	private void Update()
	{
		if(_isActive)
		{
			Quaternion goalRot = (_isOpening == true) ? openQuat : startQuat;
			this.transform.localRotation = Quaternion.Lerp(
				this.transform.localRotation,
				goalRot,
				rotationSpeed * Time.deltaTime);
			if(Quaternion.Angle(this.transform.localRotation, goalRot) < 1f)
			{
				if (_callback != null)
				{
					_callback();
					_callback = null;
				}
				_isActive = false;
			}
		}
	}

	public void ToggleDoor(Action callback)
	{
		_isOpening = !_isOpening;
		_isActive = true;
		_callback = callback;
		_audioSource.clip = openClips[UnityEngine.Random.Range(0, openClips.Length - 1)];
		_audioSource.Play();
	}
}