﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class RoomManager : MonoBehaviour
{
	public float roomTimer = 60.0f;
	public bool isActive { get; set; }

    private GameObject _canvasObject;

    [HideInInspector]
    public AnalogItem correctAudioItem;
    [HideInInspector]
    public List<AudioClip> incorrectAudioClips;

	private float _roomTimer;
	private PlayerController _player;

    public int correctDoorID;

	private void Awake()
	{
		_player = GameObject.FindObjectOfType<PlayerController>();
        _canvasObject = GameObject.Find("Canvas");
        isActive = false;
	}

    public void StartRoomChallenge()
	{
		AnalogSoundManager.SetAudioClips(incorrectAudioClips.Concat(new[] { correctAudioItem.audioClip }).ToArray());
		_player.isActive = true;
		_roomTimer = roomTimer;
		isActive = true;
        this._canvasObject.GetComponent<Canvas>().enabled = true;
		GameManager.SetCurrentRoomManager(this);
        RoomFactory.AppendRoom(this.transform.parent.parent.gameObject, correctDoorID);
	}

	public void EndRoomChallenge()
	{
		_player.isActive = false;
		isActive = false;
        AnalogSoundManager.RemoveAudioClips();
		GameManager.SetCurrentRoomManager(null);
        this._canvasObject.GetComponent<Canvas>().enabled = false;
	}

	public void MakeDoorSelection(int doorID)
	{
        Transform path = null;
        switch (doorID)
        {
            case 1:
                path = this.transform.parent.Find("Pathing Nodes/LeftPath");
                break;
            case 2:
                path = this.transform.parent.Find("Pathing Nodes/MiddlePath");
                break;
            case 3:
                path = this.transform.parent.Find("Pathing Nodes/RightPath");
                break;
        }
        PathingManager.SetNextNode(path.GetChild(0).GetComponent<PathingNode>());
        PathingManager.path = true;
        EndRoomChallenge();      
	}

	public float GetTimerRemaining()
	{
		return (isActive == false) ? GetInitialTimer() : _roomTimer;
	}

	public float GetInitialTimer()
	{
		return roomTimer;
	}

	private void Update()
	{
		if(isActive == true)
		{
			_roomTimer -= Time.deltaTime;
			if(_roomTimer <= 0.0f)
			{
				TriggerTimerComplete();
			}
		}
	}

	private void TriggerTimerComplete()
	{
        //Do stuff
        if (GameManager.IsGameStarted() == true)
        {
            GameManager.GameLost();
        }
	}

}