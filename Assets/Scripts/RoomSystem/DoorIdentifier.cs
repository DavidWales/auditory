﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorIdentifier : MonoBehaviour
{
    public Vector3 StartingPosition;
    public Vector3 StartingRotation;
    public Vector3 StartingScale = new Vector3(1f, 1f, 1f);
}