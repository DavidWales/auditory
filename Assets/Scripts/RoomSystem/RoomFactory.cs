﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomFactory : MonoBehaviour
{
    public GameObject roomPrefab;
    public List<GameObject> doorIdentifierObjects;
    public Transform environment;

    private GameObject _currentRoom;
    private GameObject _nextRoom;

    public static void AppendRoom(GameObject currentRoom, int doorID)
    {
        string roomSpawnPointName = (doorID == 1)
                                    ? "LeftDoorRoomAnchor"
                                    : (doorID == 2)
                                        ? "MiddleDoorRoomAnchor"
                                        : "RightDoorRoomAnchor";
        Transform roomSpawnPoint = currentRoom.transform.Find("Management/Room Anchors/" + roomSpawnPointName);
        GameObject newRoom = GameObject.Instantiate(_inst.roomPrefab, roomSpawnPoint.position, roomSpawnPoint.rotation);
        newRoom.transform.SetParent(_inst.environment);

        if(_inst._currentRoom != null)
        {
            CleanRoom(_inst._currentRoom);
        }
        _inst._currentRoom = _inst._nextRoom;
        _inst._nextRoom = newRoom;

        SetupRoom(newRoom);
    }

    public static void SetupRoom(GameObject newRoom)
    {
        AnalogItem correctAnalogItem = AnalogSoundManager.GetCorrectAnalogItem();
        List<AudioClip> incorrectAnalogClips = AnalogSoundManager.GetIncorrectAudioClips();
        RoomManager newRoomManager = newRoom.GetComponentInChildren<RoomManager>();
        newRoomManager.correctAudioItem = correctAnalogItem;
        newRoomManager.incorrectAudioClips = incorrectAnalogClips;

        List<GameObject> usedDoorIdentifiers = new List<GameObject>();
        usedDoorIdentifiers.Add(correctAnalogItem.doorIdentifier);

        List<int> doors = new List<int>{ 1, 2, 3 };
        int correctDoor = Random.Range(1, 4);
        newRoomManager.correctDoorID = correctDoor;
        doors.Remove(correctDoor);

        _inst.AddItemByDoorID(correctAnalogItem.doorIdentifier, correctDoor, newRoom);
        foreach(int doorId in doors)
        {
            GameObject newObj = _inst.GetUniqueDoorIdentifier(usedDoorIdentifiers);
            _inst.AddItemByDoorID(newObj, doorId, newRoom);
            usedDoorIdentifiers.Add(newObj);
        }

        if (_inst._currentRoom != null)
        {
            RoomManager currentRoomManager = _inst._currentRoom.GetComponentInChildren<RoomManager>();
            string correctPath = (currentRoomManager.correctDoorID == 1)
                                    ? "LeftPath"
                                    : (currentRoomManager.correctDoorID == 2)
                                        ? "MiddlePath"
                                        : "RightPath";
            Transform path = _inst._currentRoom.transform.Find("Management/Pathing Nodes/" + correctPath);
            PathingNode doorNode = path.GetChild(path.childCount - 1).GetComponent<PathingNode>();
            PathingNode nextRoomPathingNode = _inst._nextRoom.transform.Find("Management/Pathing Nodes").GetChild(0).GetComponent<PathingNode>();
            doorNode.nextNode = nextRoomPathingNode;
        }
    }

    public static void CleanRoom(GameObject room)
    {
        room.SetActive(false);
    }

    private void AddItemByDoorID(GameObject item, int doorId, GameObject newRoom)
    {
        string itemLocationLocName = (doorId == 1)
                                        ? "LeftDoorIdentifier"
                                        : (doorId == 2)
                                            ? "MiddleDoorIdentifier"
                                            : "RightDoorIdentifier";
        Transform doorItemLoc = newRoom.transform.Find("Door Identifiers/" + itemLocationLocName);
        GameObject newItem = GameObject.Instantiate(item, Vector3.zero, Quaternion.identity);
        newItem.transform.SetParent(doorItemLoc);

        DoorIdentifier doorIdentifier = newItem.GetComponent<DoorIdentifier>();
        newItem.transform.localPosition = doorIdentifier.StartingPosition;
        newItem.transform.localRotation = Quaternion.Euler(doorIdentifier.StartingRotation);
        newItem.transform.localScale = doorIdentifier.StartingScale;
    }

    private GameObject GetUniqueDoorIdentifier(List<GameObject> usedDoorIdentifiers)
    {
        while (usedDoorIdentifiers.Count < doorIdentifierObjects.Count)
        {
            GameObject potentialNewObj = doorIdentifierObjects[Random.Range(0, doorIdentifierObjects.Count - 1)];
            if(usedDoorIdentifiers.Contains(potentialNewObj) == false)
            {
                return potentialNewObj;
            }
        }
        return null;
    }

    #region Singleton Stuff

    public static RoomFactory _inst = null;

    void Awake()
    {
        if (_inst != null)
        {
            Destroy(this.gameObject);
            return;
        }
        _inst = this;
        _nextRoom = environment.GetChild(0).gameObject;
    }

    #endregion
}