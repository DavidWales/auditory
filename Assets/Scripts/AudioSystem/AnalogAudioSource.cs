﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnalogAudioSource
{
	public AudioSource audioSource;
	public Vector2 positionRelativeToPlayer;
}