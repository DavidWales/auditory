﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class AnalogSoundManager : MonoBehaviour
{
	public float distanceFromPlayer = 5.0f;
	public GameObject audioSourcePrefab;
	public Transform player;
	public float audioLerpSpeed = 1.0f;
	public float enhancedVolumePoint = 0.85f;
	public float dampendVolume = 0.05f;
    public List<AnalogItem> correctAnalogItems;
    public List<AudioClip> incorrectAnalogClips;

	private List<AnalogAudioSource> _analogAudioSources = new List<AnalogAudioSource>();
	private SignalDetectedReticle _signalDetectedReticle;
	private AudioClip[] _audioClips;

	public static void SetAudioSourcePropertiesByAnalog(Vector2 analogPos)
	{
		if (_inst._analogAudioSources.Count > 0)
		{
			float loudestVolume = _inst._analogAudioSources.Max(a => 1.0f - (((a.positionRelativeToPlayer - analogPos).magnitude) / 2.0f));

			bool isEnhancingSignal = false;
			foreach (AnalogAudioSource analogAudioSource in _inst._analogAudioSources)
			{
				//Transition from current volume to new volume (based on distance from analog position)
				float desiredVolume = 1.0f - (((analogAudioSource.positionRelativeToPlayer - analogPos).magnitude) / 2.0f);
				if (loudestVolume > _inst.enhancedVolumePoint && desiredVolume < loudestVolume)
				{
					desiredVolume = _inst.dampendVolume;
					isEnhancingSignal = true;
				}
				analogAudioSource.audioSource.volume = Mathf.Lerp(
						analogAudioSource.audioSource.volume,
						desiredVolume,
                        _inst.audioLerpSpeed * Time.deltaTime);
			}

			if (isEnhancingSignal == true)
			{
                _inst._signalDetectedReticle.SetReticleActive();
			}
			else
			{
                _inst._signalDetectedReticle.SetReticleInactive();
			}
		}
	}

	public static void SetAudioClips(AudioClip[] newAudioClips)
	{
		RemoveAudioClips();
		_inst._audioClips = newAudioClips;
		CreateAudioSources();
	}

	public static void RemoveAudioClips()
	{
		if (_inst._analogAudioSources.Any())
		{
			foreach (AnalogAudioSource analogAudioSource in _inst._analogAudioSources)
			{
				GameObject.Destroy(analogAudioSource.audioSource.gameObject);
			}
            _inst._analogAudioSources.Clear();
		}
	}

	private static void CreateAudioSources()
	{
		Quaternion angleBetween = Quaternion.Euler(0.0f, 0.0f, 360.0f / _inst._audioClips.Length);
		Vector3 audioSourcePosition = Vector3.up * _inst.distanceFromPlayer;
		foreach (AudioClip clip in _inst._audioClips)
		{
			//Calculate position of next audio source
			audioSourcePosition = angleBetween * audioSourcePosition;

			//Instantiate audio source prefab as child of player
			GameObject newAudioSourceGameObj = GameObject.Instantiate(_inst.audioSourcePrefab, Vector3.zero, Quaternion.identity);
			newAudioSourceGameObj.transform.SetParent(_inst.player);
			newAudioSourceGameObj.transform.localPosition = audioSourcePosition;

			//Set the clip of the new audio source to the designated clip
			AudioSource newAudioSource = newAudioSourceGameObj.GetComponent<AudioSource>();
			newAudioSource.clip = clip;
			newAudioSource.Play();

            //Add audio source to list of analog audio sources to be tracked
            _inst._analogAudioSources.Add(new AnalogAudioSource
			{
				audioSource = newAudioSource,
				positionRelativeToPlayer = newAudioSourceGameObj.transform.localPosition.normalized
			});
		}
	}

    public static AnalogItem GetCorrectAnalogItem()
    {
        return _inst.correctAnalogItems[Random.Range(0, _inst.correctAnalogItems.Count - 1)];
    }

    public static List<AudioClip> GetIncorrectAudioClips()
    {
        List<AudioClip> result = new List<AudioClip>();

        List<int> usedClipIDs = new List<int>();
        while(result.Count < GamePlaySettings.NUM_INCORRECT_ANALOG_CLIPS)
        {
            int id = Random.Range(0, _inst.incorrectAnalogClips.Count - 1);
            if(usedClipIDs.Contains(id) == false)
            {
                result.Add(_inst.incorrectAnalogClips[id]);
                usedClipIDs.Add(id);
            }
        }

        return result;
    }

    #region Singleton Stuff

    public static AnalogSoundManager _inst = null;

    void Awake()
    {
        if (_inst != null)
        {
            Destroy(this.gameObject);
            return;
        }
        _inst = this;
        _signalDetectedReticle = GameObject.FindObjectOfType<SignalDetectedReticle>();
    }

    #endregion
}